import React, { Component } from 'react';
import * as THREE from "three";

import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

// Loaders
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';

// Support Libraries
import * as dat from 'dat.gui';
import Stats from 'stats.js';

class CanvasHome extends Component {
    
    constructor(props) {
        super(props);
        this.scene = null;
        this.camera = null;
        this.renderer = null;
        this.sunLight = null;
        this.frameId = null;
        this.controls = null;
        this.stats = null;
        this.gui = null;
        this.clock = null;
        this.loadManager = null;

        this.gltfLoader = null;

        this.WNDSIZE = { width:0, height: 0};

        this.state = {
            // Temp
        }

        window.myThis = this;
    }

    componentDidMount() {
        // CREATE SCENE
        this.initialize();
    }

    componentWillUnmount() {
        // Stop render loop
        this.uninitialize();
        this.stop();
    }


    initialize = () => {

        const scope = this;

        // GUI Tools
        this.gui = new dat.GUI({width: 300, height: 300});
        this.stats = new Stats();
        this.stats.showPanel(0);
        document.body.appendChild(this.stats.dom);

        this.loadManager = new THREE.LoadingManager(this.onLoad, this.onProgess);

        this.scene = new THREE.Scene();

        /* Window Size [ Set Up Render onto our canvas] */ 
        this.WNDSIZE.width = window.innerWidth;
        this.WNDSIZE.height = window.innerHeight;
        
        //Create Renderer
        if(window.devicePixelRatio >= 2) {
            this.renderer = new THREE.WebGLRenderer({ alpha: true });
        } else {
            this.renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
        }
        this.renderer.setClearColor('#211d20');
        this.renderer.setSize(this.WNDSIZE.width, this.WNDSIZE.height);
        this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
        this.mount.appendChild(this.renderer.domElement);

        // Object SetUP
        this.gltfLoader = new GLTFLoader(this.loadManager);
        this.gltfLoader.load('/assets/models/pasadena_auditorium/scene.gltf', (gltf) => {
            const obj = gltf.scene.children[0].children[0].children[0];
            obj.children.forEach((child) => {
                if(child instanceof THREE.Mesh) {
                    child.material.map.wrapS = child.material.map.wrapT = THREE.RepeatWrapping;
                    child.material.map.minFilter = child.material.map.magFilter = THREE.NearestFilter;
                    child.material.map.generateMipmaps = false;
                }
            })
            gltf.scene.position.set(50, 0, 0);

            const folder2 = scope.gui.addFolder('Model');
            folder2.add(gltf.scene.position, 'x', 0, 120, 1).name("PositionX");
            folder2.add(gltf.scene.position, 'y', -10, 10, 1).name("PositionY");
            folder2.add(gltf.scene.position, 'z', -10, 10, 1).name("PositionZ");

            scope.scene.add(gltf.scene);
        })

        // Dir Light to the scene
        this.sunLight = new THREE.DirectionalLight('#ffffff', 1);
        this.sunLight.position.set(0.25, 3, - 2.25)
        this.scene.add(this.sunLight);

        const folder = this.gui.addFolder('Sun Light');
        folder.add(this.sunLight, 'intensity', 1, 10, 0.001).name('sunLight Intensity');
        folder.add(this.sunLight.position, 'x', -5, 5, 0.001).name('sunLightX');
        folder.add(this.sunLight.position, 'y', -5, 5, 0.001).name('sunLightY');
        folder.add(this.sunLight.position, 'z', -5, 5, 0.001).name('sunLightZ');
        
        this.setupCamera();
        this.clock = new THREE.Clock();

        // Start Animation
        this.start();

        /* Handle Resize */
        window.addEventListener("resize", this.resizeWindow);

    }

    onLoad = () => {
        console.log("Success");
    }

    onProgess = (s, c, t) => {
        // console.log(c, t);
    }


    setupCamera = () => {
        this.camera = new THREE.PerspectiveCamera(60, (this.WNDSIZE.width/this.WNDSIZE.height), 0.1, 1000);
        this.camera.position.set(4, 1, - 4);
        this.camera.lookAt(this.scene.position);

        /* Orbit Cotrols */
        this.controls = new OrbitControls( this.camera, this.renderer.domElement );
        this.controls.enableDamping = true;
    }

    start = () => {
        // if already initalized then leave it be
        if(!this.frameId) {
            this.frameId = requestAnimationFrame(this.update);
        }
    }

    stop = () => {
        cancelAnimationFrame(this.frameId);
    };

    resizeWindow = () => {
        /* Window Size */
        this.WNDSIZE.width = window.innerWidth;
        this.WNDSIZE.height = window.innerHeight;

        this.camera.aspect = this.WNDSIZE.width / this.WNDSIZE.height;
        this.camera.updateProjectionMatrix();

        this.renderer.setSize( this.WNDSIZE.width, this.WNDSIZE.height );
        this.renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2));
    }


    update = () => {

        this.stats.begin();

        const elapsedTime = this.clock.getElapsedTime();

        this.controls.update();
        this.renderScene();

        this.frameId = window.requestAnimationFrame(this.update);
        
        this.stats.end();
    }

    renderScene = () => {
        let { renderer, scene, camera, } = this;
        if(renderer) {
            renderer.render(scene, camera);
        }
    }

    uninitialize = () => {
        this.scene.traverse((child) => {
            if(child instanceof THREE.Mesh) {
                child.geometry.dispose();
                for(const key in child.material) {
                    const value = child.material[key];
                    if(value && typeof value === 'function') {
                        value.dispose();
                    }
                }
            }
        });

        this.controls.dispose();
        this.renderer.dispose();

        if(this.gui) {
            this.gui.destroy();
        }
    }

    render() {
        return (
            <div ref={ref => (this.mount = ref)} className="canvasContainer__maincanvas"/>
        )
    }
}

export default CanvasHome;
